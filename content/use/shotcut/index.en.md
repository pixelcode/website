---
title: Shotcut
icon: icon.png
replaces: 
    - adobe-cloud
---

**Shotcut** is a free [open][floss] NLE video editor for Windows, Mac and Linux, available from the official website.

{{< infobox >}}
- **Website:**
    - [shotcut.org](https://www.shotcut.org/)
    - [Download](https://www.shotcut.org/download/)
{{< /infobox >}}

[floss]: {{< relref "/articles/free-libre-open-software" >}}