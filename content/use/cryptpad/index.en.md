---
title: CryptPad
icon: icon.png
replaces:
    - google-drive
---

**CryptPad** is an [open source][1] privacy-friendly alternative to Google Docs. It lets you easily edit many kinds of documents online and collaboratively, and is based on the “zero knowledge” concept where no one except you (and whoever you give your access keys to) can access your document.

The service is free up to a certain storage limit, with paid accounts for additional storage. You can even use the free service without registering if you want, though it’s worth registering as it lets you keep documents permanently online.

{{< infobox >}}
- **Website:** 
    - [cryptpad.fr](https://cryptpad.fr/)
{{< /infobox >}}

[1]: https://web.archive.org/web/20180904102804/https://switching.social/what-is-open-source-software/
