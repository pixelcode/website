---
title: Adobe Creative Cloud
subtitle: Design Software
provider: adobe
order: 
    - gimp
    - glimpse-editor
    - inkscape
    - krita
    - darktable
    - rawtherapee
    - blender
    - kdenlive
    - shotcut
    - flowblade
    - scribus
aliases:
    - /ethical-alternatives-to-adobe-creative-cloud/
---

Adobe’s Creative Cloud suite of creative apps is so-called “software as a service”, which means you have to pay rent on it every month or lose access to your work. Adobe gets to set how much the rent is, and once you’re sucked into the system there’s nothing to stop [Adobe increasing prices][adobe-prices] to whatever they want.

Alternatively, you could try using [free and open][floss] software that you own completely, with no time limits and no rent payments to make. The free alternatives aren’t an exact match for Creative Cloud’s components, but you can take a look at the list below and see if what is available matches your needs.

[adobe-prices]: https://www.gizmodo.com.au/2017/05/creative-cloud-keeps-getting-more-expensive-in-australia/
[floss]: {{< relref "/articles/free-libre-open-software" >}}